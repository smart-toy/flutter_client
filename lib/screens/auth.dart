import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:smart_toy_client/models/child.model.dart';
import 'package:smart_toy_client/screens/wait-for-the-task.dart';

class Auth extends StatefulWidget {
  const Auth({Key? key}) : super(key: key);

  @override
  State<Auth> createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  ValueNotifier<dynamic> result = ValueNotifier(null);

  void _readNfcTag() {
    NfcManager.instance.startSession(onDiscovered: (NfcTag tag) async {
      var ndef = Ndef.from(tag);
      if (ndef != null) {
        var ndefMessage = ndef.cachedMessage!;
        if (ndefMessage.records.isNotEmpty &&
            ndefMessage.records.first.typeNameFormat ==
                NdefTypeNameFormat.nfcWellknown) {
          //If the first record exists as 1:Well-Known we consider this tag as having a value for us
          final wellKnownRecord = ndefMessage.records.first;

          if (wellKnownRecord.payload.first == 0x02) {
            //Now we know the encoding is UTF8 and we can skip the first byte
            final languageCodeAndContentBytes =
                wellKnownRecord.payload.skip(1).toList();
            final languageCodeAndContentText =
                utf8.decode(languageCodeAndContentBytes);
            //Cutting of the language code
            final childId = languageCodeAndContentText.substring(2);
            result.value = childId;
          }
        }
      }
    });
  }

  void _logIn(String child) {
    NfcManager.instance.stopSession();

    // TODO: авторизация
    // предлагаю не использовать стейт менеджеры, а просто таскать стейт из виджета в виджет ))


    // эту козявку получили с сервера
    final currentChild = Child(id: '1', groupId: '1', name: child, secondName: 'Фамилия', teacherName: 'Мария Ивановна');

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => WaitForTheTask(child: currentChild)),
    );
  }

  @override
  Widget build(BuildContext context) {

    _readNfcTag();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Привет!')),
        body: SafeArea(
          child: Center(
            child: Column(
              children: [
                Expanded(
                  child: FutureBuilder<bool>(
                    future: NfcManager.instance.isAvailable(),
                    builder: (context, ss) => ss.data != true
                        ? const Text("Ошибка с модулем NFC")
                        : ValueListenableBuilder<dynamic>(
                            valueListenable: result,
                            builder: (context, value, _) => value == null
                                ? const Text(
                                    'Приложи свой ключ к устройству',
                                    style: TextStyle(fontSize: 24.0),
                                  )
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Привет! Тебя зовут $value?',
                                        style: const TextStyle(fontSize: 24.0),
                                      ),
                                      const SizedBox(height: 16.0),
                                      ElevatedButton(
                                        onPressed: () => _logIn(result.value),
                                        child: const Padding(
                                          padding: EdgeInsets.all(12.0),
                                          child: Text(
                                            'Войти',
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 16.0),
                                    ],
                                  ),
                          ),
                  ),
                ),
                Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                      onPressed: () => _logIn('Лиза'),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Войти как Лиза',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () => _logIn('Артем'),
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'Войти как Артем',
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
