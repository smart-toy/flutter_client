import 'dart:convert';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'package:volume_controller/volume_controller.dart';

class Task extends StatefulWidget {
  const Task({Key? key}) : super(key: key);

  @override
  State<Task> createState() => _TaskState();
}

class _TaskState extends State<Task> {
  List _taskData = [];

  List<List<String>> _answers = [['startDatetime', 'answerDatetime', 'task', 'quesion', 'answer', 'rightAnswer']];
  DateTime _startTime = DateTime.now();

  int _currentText = 0;
  int _currentQuestion = 0;
  bool _showVariants = false;
  bool _completed = false;

  int? _highlightedAnswer;
  bool _right = false;

  final playerText = AudioPlayer();
  final playerQuestion = AudioPlayer();
  final playerAnswer = AudioPlayer();

  Future<void> readTaskData() async {
    final String response =
        await rootBundle.loadString('assets/data/quiz-15-03-2023.data.json');
    final data = await jsonDecode(response);

    setState(() {
      _taskData = data['data'];
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await readTaskData();
      // запустить первый трек
      await playerText.play(AssetSource(_taskData[0]['taskSource']));

      // скорость для теста
      // await playerText.setPlaybackRate(1);
      // await playerQuestion.setPlaybackRate(1);
      // await playerAnswer.setPlaybackRate(1);

      playerText.onPlayerComplete.listen((event) {
        print('TEXT COMPLETED');
        setState(() {
          playerQuestion.play(AssetSource(_taskData[_currentText]['questions']
              [_currentQuestion]['questionSource']));
        });
      });

      playerQuestion.onPlayerComplete.listen((event) {
        print('QUESTION COMPLETED');
        setState(() {
          _showVariants = true;
          _startTime = DateTime.now();
        });
      });

      playerAnswer.onPlayerComplete.listen((event) async {
        print('ANSWER COMPLETED');
        setState(() {
          _showVariants = false;
          _currentQuestion++;
          if (_currentQuestion == _taskData[_currentText]['questions'].length) {
            _currentText++;
            _currentQuestion = 0;
            if (_currentText == _taskData.length) {
              _completed = true;
            }
          }
        });
        if (_completed) {
          String csv = const ListToCsvConverter(fieldDelimiter: ';').convert(_answers);
          var date = DateFormat('dd-MM-yyyy hh-mm-ss').format(DateTime.now());
          var path = '/storage/emulated/0/Download/$date-results.csv';
          var file = await File(path).create();
          await file.writeAsString(csv);
          return;
        }
        await playerQuestion.setSource(AssetSource(_taskData[_currentText]
            ['questions'][_currentQuestion]['questionSource']));
        await playerAnswer.setSource(AssetSource(_taskData[_currentText]
            ['questions'][_currentQuestion]['rightAnswerSource']));
        await playerText
            .setSource(AssetSource(_taskData[_currentText]['taskSource']));

        if (_currentQuestion != 0) {
          await playerQuestion.resume();
        } else {
          playerText.resume();
        }
      });
    });
  }

  void _repeatText() async {
    setState(() {
      _showVariants = false;
      _right = false;
      _highlightedAnswer = null;
    });
    await playerText.play(AssetSource(_taskData[_currentText]['taskSource']));
  }

  void _repeatQuestion() async {
    setState(() {
      _showVariants = false;
      _right = false;
      _highlightedAnswer = null;
    });
    await playerQuestion.play(AssetSource(_taskData[_currentText]['questions']
        [_currentQuestion]['questionSource']));
  }

  void _checkAnswer(int answerIndex) {
    final answer = _taskData[_currentText]['questions'][_currentQuestion]
        ['answers'][answerIndex];
    final rightAnswer =
        _taskData[_currentText]['questions'][_currentQuestion]['rightAnswer'];
    final taskText = _taskData[_currentText]['taskText'];
    final questionText = _taskData[_currentText]['questions'][_currentQuestion]['questionText'];

    setState(() {
      _highlightedAnswer = answerIndex;
      _answers.add([_startTime.toString(), DateTime.now().toString(), taskText, questionText, answer.toString(), rightAnswer.toString()]);
    });
    if (answer == rightAnswer) {
      setState(() {
        _right = true;
      });
      playerAnswer.play(AssetSource(_taskData[_currentText]['questions']
          [_currentQuestion]['rightAnswerSource']));
      Future.delayed(const Duration(seconds: 2), () {
        setState(() {
          _highlightedAnswer = null;
        });
      });
    } else {
      setState(() {
        _right = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('4 Мышонка'),
        backgroundColor: !_showVariants ? Colors.blue : _currentQuestion % 2 == 0 ? Colors.amber : Colors.indigo,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            playerQuestion.stop();
            playerText.stop();
            playerAnswer.stop();
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.volume_down),
            onPressed: () async {
              VolumeController().setVolume(await VolumeController().getVolume() + 0.1);
            },
          ),
          IconButton(
            icon: const Icon(Icons.volume_up),
            onPressed: () async {
              VolumeController().setVolume(await VolumeController().getVolume() - 0.1);
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            if (!_showVariants & !_completed)
              const SpinKitChasingDots(
                color: Colors.blue,
                size: 80.0,
              ),
            if (_showVariants) ...[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  OutlinedButton(
                    onPressed: () => _checkAnswer(0),
                    style: OutlinedButton.styleFrom(
                      backgroundColor: _currentQuestion % 2 == 0 ? Colors.amber : Colors.indigo,
                      side: BorderSide(
                          color: (_highlightedAnswer == 0)
                              ? ((_right) ? Colors.green : Colors.red)
                              : Colors.black12,
                          width: (_highlightedAnswer == 0) ? 6 : 1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 36.0, vertical: 12.0),
                      child: Text(
                        _taskData[_currentText]['questions'][_currentQuestion]
                                ['answers'][0]
                            .toString(),
                        style: const TextStyle(fontSize: 26.0, color: Colors.white),
                      ),
                    ),
                  ),
                  const SizedBox(width: 48.0),
                  OutlinedButton(
                    onPressed: () => _checkAnswer(1),
                    style: OutlinedButton.styleFrom(
                      backgroundColor: _currentQuestion % 2 == 0 ? Colors.amber : Colors.indigo,
                      side: BorderSide(
                          color: (_highlightedAnswer == 1)
                              ? ((_right) ? Colors.green : Colors.red)
                              : Colors.black12,
                          width: (_highlightedAnswer == 1) ? 6 : 1),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 36.0, vertical: 12.0),
                      child: Text(
                        _taskData[_currentText]['questions'][_currentQuestion]
                                ['answers'][1]
                            .toString(),
                        style: const TextStyle(fontSize: 26.0, color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 32),
              ElevatedButton(
                onPressed: _repeatQuestion,
                style: ElevatedButton.styleFrom(
                  backgroundColor: _currentQuestion % 2 == 0 ? Colors.amber : Colors.indigo,
                ),
                child: const Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: 36.0, vertical: 12.0),
                  child: Text(
                    'Послушать еще раз',
                    style: TextStyle(fontSize: 14.0),
                  ),
                ),
              ),
            ],
            if (_completed) ...[
              const Center(
                  child: Text(
                'Поздравляем, вы справились! Позовте воспитателя',
                style: TextStyle(fontSize: 22),
              )),
              const SizedBox(height: 24.0),
              Center(
                child: OutlinedButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 36.0, vertical: 12.0),
                    child: Text(
                      'Закончить',
                      style: TextStyle(fontSize: 26.0),
                    ),
                  ),
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
