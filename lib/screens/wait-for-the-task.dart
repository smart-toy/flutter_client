import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:smart_toy_client/models/child.model.dart';
import 'package:smart_toy_client/screens/task.dart';

class WaitForTheTask extends StatefulWidget {
  const WaitForTheTask({Key? key, required this.child}) : super(key: key);

  final Child child;

  @override
  State<WaitForTheTask> createState() => _WaitForTheTaskState();
}

class _WaitForTheTaskState extends State<WaitForTheTask> {
  void startTask(int taskNum) {
    if (taskNum == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Task()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Привет!')),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Пожалуйста, поделитесь на 2 команды',
                style: TextStyle(fontSize: 22.0),
              ),
              const SizedBox(height: 16.0),
              const SpinKitChasingDots(
                color: Colors.blue,
                size: 80.0,
              ),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () => startTask(1),
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 36)),
                child: const Text(
                  'Начать урок',
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
